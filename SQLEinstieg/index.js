// Mein Nachname, mein Vorname
'use strict'

let nameDerDatenbank = "meineDatenbank94";

class Auto{
    constructor(){
        this.leistungInPs;   
        this.farbe = "grün";
        this.leistungInKw;
    }
  
    psNachKwUmrechnen(){
        return 0.735499 * parseFloat(this.leistungInPs);
    }

    ausgabe(){                
        return "Farbe: " + this.farbe + " Leistung: " + this.leistungInPs + " PS (" + this.leistungInKw + " kW)";
    }
} // Ende der Klassendefintion

let auto = new Auto();

function btnEinAutoAnlegenOnClick() {
    auto.leistungInPs = parseFloat(document.getElementById('tbxPs').value);        
    auto.farbe = document.getElementById('tbxFarbe').value;
    auto.leistungInKw = auto.psNachKwUmrechnen();    
    console.log(auto);
    console.log("INSERT INTO Autos ...")
    db.run("INSERT INTO Autos(AID, leistungInPs, farbe) VALUES (?,?,?)", [1, auto.leistungInPs, auto.farbe]);
    console.log("Nach INSERT, UPDATE oder DELETE muss die Datenbank jetzt auf die Festplatte geschrieben werden ...");    
    window.localStorage.setItem(nameDerDatenbank, datenbankAufFestplatteSchreiben(db.export()));        
}

function btnEinAutoAnzeigenOnClick(){    
    document.getElementById("lblAusgabe").innerHTML = auto.ausgabe();
    console.log(auto);
}

function btnAlleAutosAnzeigenOnClick(){    
    
    let abfrageErgebnis = db.prepare("SELECT * FROM Autos WHERE farbe='grün'");    
    let autoListe = [];
    
    while(abfrageErgebnis.step()) {
        var row = abfrageErgebnis.getAsObject();         
        
        let a = new Auto();
        a.leistungInPs = row.leistungInPs;
        a.leistungInKw = a.psNachKwUmrechnen();        
        a.farbe = row.farbe;        
        autoListe.push(a)        
    }   
    console.log(autoListe);
    console.log("Beispielhaft wird die Farbe des ersten Autos ausgelesen: " + autoListe[0].farbe)

    let ausgabestring = "";

    for (var i = 0; i < autoListe.length; i++) {        
        console.log(autoListe[i]);
        ausgabestring += autoListe[i].ausgabe() + "<br>";
    }
    document.getElementById("lblAusgabeDatenbank").innerHTML = ausgabestring;
}

function btnAlleEigenschaftenAllerAutosOnClick()
{
    let abfrageErgebnis = db.prepare("SELECT * FROM Autos");        
    let liste = [];    
    while(abfrageErgebnis.step()) {
        var row = abfrageErgebnis.getAsObject();         
        let a = new Auto();
        a.leistungInPs = row.leistungInPs;
        a.leistungInKw = a.psNachKwUmrechnen();        
        a.farbe = row.farbe;        
        liste.push(a)     
    }
    console.log(liste);

    let titel = document.getElementById("AlleEigenschaftenAllerAutos").value

    tabelleAnzeigen(titel, abfrageErgebnis)
}

function tabelleAnzeigen(titel, abfrageErgebnis){
    let ausgabestring = titel +"<br>"
    let kopfzeile = ""
    while(abfrageErgebnis.step()) {
        var row = abfrageErgebnis.getAsObject()             
        ausgabestring += "<tr>"
        kopfzeile = "<tr>"
        Object.keys(row).forEach(function(column) {
            ausgabestring += "<td>"                
            ausgabestring += row[column]
            ausgabestring += "</td>"
            kopfzeile += "<td>"                
            kopfzeile += column
            kopfzeile += "</td>"
        });
        ausgabestring += "</tr>"
        kopfzeile += "</tr>"
    }   

    ausgabestring = "<table border='6'>" + kopfzeile + ausgabestring + "</table>"
    document.getElementById("lblAusgabeDatenbank").innerHTML = ausgabestring;
}


// Ab hier kommt die Datenbankfunktionalität.

let sql = require('./sql.js'); // Das SQLite-Datenbank-Modul wird eingebunden.

let db;

if(localStorage.getItem(nameDerDatenbank) == null){
    console.log("Es existiert noch keine Datenbank. Sie wird jetzt erstellt ...");
    db = new sql.Database();
}
else{
    console.log("Die existierende Datenbank wird geöffnet ...")     
    db = new sql.Database(datenbankVonFestplatteLaden(localStorage.getItem(nameDerDatenbank)));    
}

// Eine Tabelle zur Aufnahme der von Daten wird angelegt.

db.run("CREATE TABLE IF NOT EXISTS Autos (AID int PRIMARY Key, leistungInPs int, farbe char);");
db.run("CREATE TABLE IF NOT EXISTS Personen (PID int PRIMARY Key, PLZ char, Name char);");
db.run("CREATE TABLE IF NOT EXISTS Fahrer (AID int, PID int, PRIMARY Key (AID,PID));");
db.run("INSERT OR IGNORE INTO Personen(PID, PLZ, Name) VALUES (?,?,?)", [1, "46325", "Müller"]);
db.run("INSERT OR IGNORE INTO Personen(PID, PLZ, Name) VALUES (?,?,?)", [2, "48653", "Meyer"]);
db.run("INSERT OR IGNORE INTO Autos(AID, leistungInPs, farbe) VALUES (?,?,?)", [1, 45, "grün"]);
db.run("INSERT OR IGNORE INTO Autos(AID, leistungInPs, farbe) VALUES (?,?,?)", [2, 90, "rot"]);
db.run("INSERT OR IGNORE INTO Autos(AID, leistungInPs, farbe) VALUES (?,?,?)", [3, 110, "blau"]);
db.run("INSERT OR IGNORE INTO Fahrer(AID, PID) VALUES (?,?)", [1, 1]);


// Diese Funktion schreibt die Datenbank als Datei auf die Festplatte
function datenbankAufFestplatteSchreiben (arr) {
    var uarr = new Uint8Array(arr);
    var strings = [], chunksize = 0xffff;
    // There is a maximum stack size. We cannot call String.fromCharCode with as many arguments as we want
    for (var i=0; i*chunksize < uarr.length; i++){
        strings.push(String.fromCharCode.apply(null, uarr.subarray(i*chunksize, (i+1)*chunksize)));
    }    
    return strings.join('');
}

// Diese Funktion lädt die Datenbank von der Festplatte in den Arbeitsspeicher
function datenbankVonFestplatteLaden (str) {
    var l = str.length, arr = new Uint8Array(l);
    for (var i=0; i<l; i++) arr[i] = str.charCodeAt(i);    
    return arr;
}

/* 
Referenzen:
===========
https://github.com/kripken/sql.js/wiki/Persisting-a-Modified-Database
https://bitbucket.org/stbaeumer/javascript
http://www.sql-und-xml.de/sql-tutorial/
*/