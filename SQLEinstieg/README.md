# ReferenzAppJavascript #

**Diese kleine ReferenzApp soll auf einfache Weise grundlegende Konstrukte der zukünftigen Programmierung im Unterricht zeigen:**

* Eingabe über eine Textbox
* Verarbeitung in einer Instanz-Methode
* Ausgabe auf einem Label infolge des Drückens einer btnOnClick()-Function
* Zugriff auf eine Datenbank

Die ReferenzApp basiert auf Javascript und [Electron](http://electron.atom.io).

Sie können Sie ReferenzApp kopieren und je nach Sachverhalt zur Grundlage anderer Apps machen. 

Ausschließlich folgende Dateien sind zur eigenen Bearbeitung vorgesehen:

- `index.html` - Einfügen von Elementen auf der Oberfläche.
- `index.js` - Ausprogrammieren der Logik.
- `index.css` - Gestaltung des Aussehens der Elementen auf der Oberfläche.

**Um das Projekt in *Visual Studio Code* zu nutzen, müssen Sie ...**

1. Visual Studio Code starten
2. die Funktionstaste `F1` drücken, um die *Eingabeaufforderung* zu öffnen
3. das Repository klonen durch Eingabe von `git clone` in der *Eingabeaufforderung*. Dann ENTER. Anschließend muss die Adresse eingeben werden:  `https://bitbucket.org/stbaeumer/javascript.git` 
4. Electron starten
5. den geklonten Ordner mit der Maus auf die ElectronApp ziehen, um die *referenzApp* automatisch zu starten
6. optional Visual Studio Code auf automatisch speichern einstellen `Datei`->`Automatisch speichern`
7. nach Änderungen im Code zur geöffneten *ReferenzApp* wechseln und dort mit der Funktionstaste `F5` die *ReferenzApp* neu laden