// Mein Nachname, mein Vorname
'use strict'

let nameDerDatenbank = "meineDatenbank6";

class Auto{
    constructor(){
        this.leistungInPs;   
        this.farbe = "grün";
        this.leistungInKw;
    }
  
    psNachKwUmrechnen(){
        return 0.735499 * parseFloat(this.leistungInPs);
    }

    ausgabe(){                
        return "Farbe: " + this.farbe + " Leistung: " + this.leistungInPs + " PS (" + this.leistungInKw + " kW)";
    }
} // Ende der Klassendefintion

let auto = new Auto();

function btnEinAutoAnlegenOnClick() {
    auto.leistungInPs = parseFloat(document.getElementById('tbxPs').value);        
    auto.farbe = document.getElementById('tbxFarbe').value;
    auto.leistungInKw = auto.psNachKwUmrechnen();    
    console.log(auto);
    console.log("INSERT INTO autoTabelle ...")
    db.run("INSERT INTO autoTabelle(leistungInPs, farbe) VALUES (?,?)", [auto.leistungInPs, auto.farbe]);
    console.log("Nach INSERT, UPDATE oder DELETE muss die Datenbank jetzt auf die Festplatte geschrieben werden ...");    
    window.localStorage.setItem(nameDerDatenbank, datenbankAufFestplatteSchreiben(db.export()));        
}

function btnEinAutoAnzeigenOnClick(){    
    document.getElementById("lblAusgabe").innerHTML = auto.ausgabe();
    console.log(auto);
}

function btnAlleAutosAnzeigenOnClick(){    
    let abfrageErgebnis = db.prepare("SELECT * FROM autoTabelle");    
    let autoListe = [];

    // Das Abfrageergebnis wird zeilenweise durchlaufen.
    while(abfrageErgebnis.step()) {
        var row = abfrageErgebnis.getAsObject();         
        let a = new Auto();
        a.leistungInPs = row.leistungInPs;
        a.leistungInKw = a.psNachKwUmrechnen();        
        a.farbe = row.farbe;        
        autoListe.push(a)        
    }   
    console.log(autoListe);
    console.log("Beispielhaft wird die Farbe des ersten Autos ausgelesen: " + autoListe[0].farbe)

    let ausgabestring = "";

    for (var i = 0; i < autoListe.length; i++) {        
        console.log(autoListe[i]);
        ausgabestring += autoListe[i].ausgabe() + "<br>";
    }
    document.getElementById("lblAusgabeDatenbank").innerHTML = ausgabestring;
}

// Ab hier kommt die Datenbankfunktionalität.

let sql = require('./sql.js'); // Das SQLite-Datenbank-Modul wird eingebunden.

let db;

if(localStorage.getItem(nameDerDatenbank) == null){
    console.log("Es existiert noch keine Datenbank. Sie wird jetzt erstellt ...");
    db = new sql.Database();
}
else{
    console.log("Die existierende Datenbank wird geöffnet ...")     
    db = new sql.Database(datenbankVonFestplatteLaden(localStorage.getItem(nameDerDatenbank)));    
}

// Eine Tabelle zur Aufnahme der von Daten wird angelegt.
db.run("CREATE TABLE IF NOT EXISTS autoTabelle (leistungInPs int, farbe char);");

// Diese Funktion schreibt die Datenbank als Datei auf die Festplatte
function datenbankAufFestplatteSchreiben (arr) {
    var uarr = new Uint8Array(arr);
    var strings = [], chunksize = 0xffff;
    // There is a maximum stack size. We cannot call String.fromCharCode with as many arguments as we want
    for (var i=0; i*chunksize < uarr.length; i++){
        strings.push(String.fromCharCode.apply(null, uarr.subarray(i*chunksize, (i+1)*chunksize)));
    }    
    return strings.join('');
}

// Diese Funktion lädt die Datenbank von der Festplatte in den Arbeitsspeicher
function datenbankVonFestplatteLaden (str) {
    var l = str.length, arr = new Uint8Array(l);
    for (var i=0; i<l; i++) arr[i] = str.charCodeAt(i);    
    return arr;
}

/* 
Referenzen:
===========
https://github.com/kripken/sql.js/wiki/Persisting-a-Modified-Database
https://bitbucket.org/stbaeumer/javascript
http://www.sql-und-xml.de/sql-tutorial/
*/